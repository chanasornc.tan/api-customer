const express = require('express');
const bodyParser = require('body-parser')
const app = express();
app.use(bodyParser.json({ limit: '50mb'}))
app.use(bodyParser.urlencoded({ limit: '50mb' , extended: true}))
app.use(bodyParser.text())
app.use(bodyParser.raw())
const route = require('./routes')(app)
app.listen(8081,() => {
    console.log("server is start in port 8081");
})