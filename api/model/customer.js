const ConDB = require('../config/connect');
const Promise = require('bluebird');
const sql = require('yesql').mysql

exports.createCustomer = (req) => {
    console.log("req.body" , req);
    return new Promise((resolve, reject) => {
        ConDB.connect()
            .then((conn) => {
                let query = sql(
                    `
                    INSERT INTO customer
                    VALUES
                    (
                        :customer_id
                        ,:first_name
                        ,:last_name
                        ,:address
                        ,:zip_code
                        ,:gender
                        ,:brith_date
                    );

                    `
                )({
                    customer_id: req.customer_id,
                    first_name: req.first_name,
                    last_name: req.last_name,
                    address: req.address,
                    zip_code: req.zip_code,
                    gender: req.gender,
                    brith_date: req.brith_date
                });
                conn.query(query, (err, res) => {
                    if (err) {
                        console.error('Error executing query', err.stack)
                        reject(err);
                    } else {
                        resolve(res.rows[0]);
                    }
                    ConDB.closeConn(conn)
                })
            })
            .catch((err) => {
                reject(err)
            })
    })
}

exports.deleteCustomer = (req) => {
    let customer_id = req
    console.log("customer_id" , customer_id);
    return new Promise((resolve, reject) => {
        ConDB.connect().then((conn) => {
            let query = sql(
                ` DELETE FROM customer WHERE customer_id = `+ customer_id +` `
            )({})
            conn.query(query, (err, res) => {
                if (err) {
                    console.error('Error executing query', err.stack)
                    reject(err);
                }else{
                    resolve(res);
                }
                ConDB.closeConn(conn)
            })
        }).catch((err) => {
            reject(err)
        });
    });
}

exports.updateCustomer = (req) => {
    console.log(req);
    return new Promise((resolve, reject) => {
        ConDB.connect()
            .then((conn) => {
                let query = sql(`
                UPDATE customer
                SET first_name = :first_name
                    ,last_name = :last_name
                    ,address = :address
                    ,zip_code = :zip_code
                    ,gender = :gender
                    ,brith_date = :brith_date
                WHERE customer_id = :customer_id;
                `
            )({
                customer_id: req.customer_id
                ,first_name: req.first_name
                ,last_name: req.last_name
                ,address: req.address
                ,zip_code: req.zip_code
                ,gender: req.gender
                ,brith_date: req.brith_date
            })
                conn.query(query, (err, res) => {
                    if (err) {
                        console.error('Error executing query', err.stack)
                        reject(err);
                    } else {
                        resolve(res.rows[0]);
                    }
                    ConDB.closeConn(conn)
                })
            })
            .catch((err) => {
                reject(err)
            })
    })
}

exports.getallCustomer = () => {
    return new Promise((resolve, reject) => {
        ConDB.connect().then((conn) => {
            let query = sql(
                `SELECT * FROM customer`
            )({})
            conn.query(query, (err, res) => {
                if (err) {
                    console.error('Error executing query', err.stack)
                    reject(err);
                }else{
                    resolve(res);
                }
                ConDB.closeConn(conn)
            })
        }).catch((err) => {
            reject(err)
        });
    });
};

exports.getCustomerById = (req) => {
    let customer_id = req
    return new Promise((resolve, reject) => {
        ConDB.connect().then((conn) => {
            let query = sql(
                `SELECT * FROM customer WHERE customer_id = `+ customer_id  +` `
            )({})
            conn.query(query, (err, res) => {
                if (err) {
                    console.error('Error executing query', err.stack)
                    reject(err);
                }else{
                    resolve(res);
                }
                ConDB.closeConn(conn)
            })
        }).catch((err) => {
            reject(err)
        });
    });
};