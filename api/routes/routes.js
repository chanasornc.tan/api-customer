const express = require('express');
const router = express.Router();
const customer = require('../controllers/customer')

router.post('/customer/create' , customer.createCustomer)
router.get('/customer/delete' , customer.deleteCustomer)
router.post('/customer/update' , customer.updateCustomer)
router.get('/customer/getall' , customer.getallCustomer)
router.get('/customer/getcustomerbyid' , customer.getCustomerById)

module.exports = router;