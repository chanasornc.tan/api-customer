const APIModel = require('../model/customer');

exports.createCustomer = (req, res) => {
    let json = {
        customer_id: req.body.customer_id,
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        address: req.body.address,
        zip_code: req.body.zip_code,
        gender: req.body.gender,
        brith_date: req.body.brith_date
    }
    APIModel.createCustomer(json).then((data) => {
        if ((data !== undefined) && (Object.values(data).length > 0)) {
            res.status(200).json({
                status: true
                ,message: "Query Success"
                ,data
            });
        }else{
            res.status(200).json({
                status: true
                ,message: "Query Success"
                ,data: []
            });
        }
    }).catch((err) => {
        res.status(404).json({
            status: false
            ,err
        });
    });
};

exports.deleteCustomer = (req, res) => {
    let customer_id = req.query.customer_id
    APIModel.deleteCustomer(customer_id).then((data) => {
        if ((data !== undefined) && (Object.values(data).length > 0)) {
            res.status(200).json({
                status: true
                ,message: "Query Success"
                ,data
            });
        }else{
            res.status(200).json({
                status: true
                ,message: "Query Success"
                ,data: []
            });
        }
    }).catch((err) => {
        res.status(404).json({
            status: false
            ,err
        });
    });
};

exports.updateCustomer = (req, res) => {
    let json = {
        customer_id: req.body.customer_id,
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        address: req.body.address,
        zip_code: req.body.zip_code,
        gender: req.body.gender,
        brith_date: req.body.brith_date
    }
    APIModel.updateCustomer(json).then((data) => {
        if ((data !== undefined) && (Object.values(data).length > 0)) {
            res.status(200).json({
                status: true
                ,message: "Query Success"
                ,data
            });
        }else{
            res.status(200).json({
                status: true
                ,message: "Query Success"
                ,data: []
            });
        }
    }).catch((err) => {
        res.status(404).json({
            status: false
            ,err
        });
    });
};

exports.getallCustomer = (req, res) => {
    APIModel.getallCustomer().then((data) => {
        if ((data !== undefined) && (Object.values(data).length > 0)) {
            res.status(200).json({
                status: true
                ,message: "Query Success"
                ,data
            });
        }else{
            res.status(200).json({
                status: true
                ,message: "Query Success"
                ,data: []
            });
        }
    }).catch((err) => {
        res.status(404).json({
            status: false
            ,err
        });
    });
};

exports.getCustomerById = (req, res) => {
    let customer_id = req.query.customer_id
    APIModel.getCustomerById(customer_id).then((data) => {
        if ((data !== undefined) && (Object.values(data).length > 0)) {
            res.status(200).json({
                status: true
                ,message: "Query Success"
                ,data
            });
        }else{
            res.status(200).json({
                status: true
                ,message: "Query Success"
                ,data: []
            });
        }
    }).catch((err) => {
        res.status(404).json({
            status: false
            ,err
        });
    });
};