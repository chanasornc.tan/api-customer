const mysql = require('mysql')
const Promise = require('bluebird');

exports.connect = () => {
	return new Promise((resolve, reject) => {
		let connection = mysql.createConnection({
			user: "root",
			password: "",
			host: "127.0.0.1",
			port: "3306",
			database: "customer",
			max: 10,
			min: 0,
			idleTimeoutMillis: 30000
		});
		resolve(connection)
	});
};

exports.closeConn = (conn) => {
	console.log('end')
	conn.end();
};
